import { PartialType } from '@nestjs/mapped-types';
import { CreateTodolistDto } from './create-todolist.dto';
import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateTodolistDto extends PartialType(CreateTodolistDto) {
  @IsString()
  @IsNotEmpty()
  name: string;
}
