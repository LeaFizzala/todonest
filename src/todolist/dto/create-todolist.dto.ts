import { IsNotEmpty, IsString } from 'class-validator';
import { Item } from 'src/items/entities/item.entity';

export class CreateTodolistDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  items: Item[];
}
