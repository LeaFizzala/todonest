import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateTodolistDto } from './dto/create-todolist.dto';
import { UpdateTodolistDto } from './dto/update-todolist.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Todolist } from './entities/todolist.entity';
import { Item } from 'src/items/entities/item.entity';

@Injectable()
export class TodolistService {
  constructor(
    @InjectRepository(Todolist)
    private todolistRepository: Repository<Todolist>,
    @InjectRepository(Item)
    private itemRepository: Repository<Item>,
  ) {}
  create(createTodolistDto: CreateTodolistDto) {
    const newList = this.todolistRepository.create(createTodolistDto);
    this.todolistRepository.save(newList);
    return newList;
  }

  async findAll() {
    const lists = await this.todolistRepository.find({
      relations: {
        items: true,
      },
    });
    return lists;
  }

  async findOne(id: number) {
    return await this.todolistRepository.findOne({
      where: { id: id },
      relations: {
        items: true,
      },
    });
  }

  async update(id: number, updateTodolistDto: UpdateTodolistDto) {
    const todoList = await this.findOne(id);
    if (todoList == null) {
      throw new NotFoundException('No todoList exists with id: ' + id);
    }
    const newTodolist = this.todolistRepository.update(id, updateTodolistDto);
    return newTodolist;
  }

  async remove(id: number) {
    const listToDelete = await this.todolistRepository.findOne({
      where: { id: id },
    });
    if (!listToDelete) {
      throw new NotFoundException('No todoList exists with this id ' + id);
    }
    this.todolistRepository.delete(id);
    return {
      status: 200,
      message: 'TodoList successfully deleted.',
    };
  }
}
