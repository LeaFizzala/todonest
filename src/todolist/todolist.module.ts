import { Module, forwardRef } from '@nestjs/common';
import { TodolistService } from './todolist.service';
import { TodolistController } from './todolist.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Todolist } from './entities/todolist.entity';
import { ItemsModule } from 'src/items/items.module';
import { ItemsController } from 'src/items/items.controller';
import { ItemsService } from 'src/items/items.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Todolist]),
    forwardRef(() => ItemsModule),
  ],
  exports: [TypeOrmModule, TodolistService],
  controllers: [TodolistController, ItemsController],
  providers: [TodolistService, ItemsService],
})
export class TodolistModule {}
