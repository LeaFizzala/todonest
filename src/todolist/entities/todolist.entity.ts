import { Column, CreateDateColumn, Entity, OneToMany, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Item } from '../../items/entities/item.entity';

@Entity()
export class Todolist {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => Item, (items) => items.todolist)
  items!: Item[];

  @CreateDateColumn() created!: Date;
  @UpdateDateColumn() updated!: Date;
}
