import { PartialType } from '@nestjs/mapped-types';
import { CreateItemDto } from './create-item.dto';

export class CompleteItemDto extends PartialType(CreateItemDto) {
  constructor(isComplete) {
    super();
    this.isCompleted = isComplete;
  }
  isCompleted: boolean;
}
