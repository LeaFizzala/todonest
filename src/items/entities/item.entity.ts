import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Todolist } from 'src/todolist/entities/todolist.entity';

@Entity('items')
export class Item {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column('varchar')
  label: string;

  @Column('boolean', {
    default: false,
  })
  isCompleted: boolean;

  @ManyToOne(() => Todolist, (todolist) => todolist.items, {
    nullable: false,
    onDelete: 'CASCADE',
  })
  todolist: Todolist;

  @JoinColumn()
  todolistId: number;

  @CreateDateColumn({ name: 'created_at' })
  created_at: Date;

  @UpdateDateColumn({ name: 'updated_at' })
  updated_at: Date;
}
