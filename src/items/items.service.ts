import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Item } from './entities/item.entity';
import { Repository } from 'typeorm';
import { CreateItemDto } from './dto/create-item.dto';
import { UpdateItemDto } from './dto/update-item.dto';
import { TodolistService } from 'src/todolist/todolist.service';

@Injectable()
export class ItemsService {
  constructor(
    @InjectRepository(Item)
    private itemRepository: Repository<Item>,
    @Inject(TodolistService)
    private todoListservice: TodolistService,
  ) {}

  async create(createItemDto: CreateItemDto) {
    const list = await this.todoListservice.findOne(createItemDto.todolistId);
    if (list == null) {
      throw new NotFoundException('Todo List not found');
    }
    const item = this.itemRepository.create(createItemDto);
    this.itemRepository.save(item);
    return item;
  }

  findAll() {
    return this.itemRepository.find();
  }

  findAllActive() {
    return this.itemRepository.findBy({
      isCompleted: false,
    });
  }

  async findOne(id: number) {
    return await this.itemRepository.findOne({
      where: { id: id },
    });
  }

  async update(id: number, updateItemDto: UpdateItemDto) {
    const item = await this.itemRepository.findOne({
      where: { id: id },
    });
    if (item == null) {
      throw new NotFoundException('Item not found');
    }
    return this.itemRepository.update(id, updateItemDto);
  }

  async remove(id: number) {
    const item = await this.itemRepository.findOne({
      where: { id: id },
    });
    if (item == null) {
      throw new NotFoundException('Item not found');
    }
    this.itemRepository.delete(id);
    return `Item number #${id} was successfully deleted.`;
  }

  async completeItem(id: number) {
    const item = await this.itemRepository.findOne({
      where: { id: id },
    });
    if (item == null) {
      throw new NotFoundException('Item not found');
    }
    item.isCompleted = !item.isCompleted;
    this.itemRepository.update(id, item);
    return item;
  }
}
