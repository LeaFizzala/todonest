import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
  HttpException,
  HttpStatus,
  NotFoundException,
} from '@nestjs/common';
import { ItemsService } from './items.service';
import { CreateItemDto } from './dto/create-item.dto';
import { UpdateItemDto } from './dto/update-item.dto';

@Controller('items')
export class ItemsController {
  constructor(private readonly itemsService: ItemsService) {}

  @Post()
  async create(@Body() createItemDto: CreateItemDto) {
    try {
      return this.itemsService.create(createItemDto);
    } catch (error) {
      return error;
    }
  }

  @Patch('/completeItem/:id')
  async complete(@Param('id', ParseIntPipe) id: number) {
    try {
      await this.itemsService.completeItem(id);
      return {
        status: 200,
        message: 'Item was succesfully updated ',
      };
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: 'Item not found with this id.',
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }

  @Get()
  async findAll() {
    try {
      await this.itemsService.findAll();
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: "Cette action n'est pas autorisée.",
        },
        HttpStatus.FORBIDDEN,
      );
    }
    return this.itemsService.findAll();
  }

  @Get('/active')
  findallActive() {
    return this.itemsService.findAllActive();
  }

  @Get(':id')
  async findOne(@Param('id', ParseIntPipe) id: number) {
    try {
      const item = await this.itemsService.findOne(id);
      if (!item) {
        throw new NotFoundException('Item not found');
      }
      return item;
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: "Item with this ID doesn't exist. ",
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }

  @Patch(':id')
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateItemDto: UpdateItemDto,
  ) {
    try {
      await this.itemsService.update(id, updateItemDto);
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: "Item with this ID doesn't exist. ",
        },
        HttpStatus.NOT_FOUND,
      );
    }
    return {
      status: 201,
      message: 'Item succesfully updated.',
    };
  }

  @Delete(':id')
  async remove(@Param('id', ParseIntPipe) id: number) {
    try {
      await this.itemsService.remove(+id);
    } catch (error) {
      throw new HttpException(
        {
          status: HttpStatus.NOT_FOUND,
          error: "Item with this ID doesn't exist. ",
        },
        HttpStatus.NOT_FOUND,
      );
    }
  }
}
